---
title: 37 Free and Royalty Free Image Banks
author: Micadep
date: 2019-04-27
hero: ./img/Images-gratuites-et-libres-de-droits.jpg
excerpt: Do you want to have in one place the best image banks for your free images, royalty-free ? 
slug: royaly-free-images
---

Do you want to have in one place the best image banks for your free images, royalty-free ? 

Then you're going to love this article.

I have listed the 37 best free and royalty-free image banks and search engines to find your vector images, illustrations and patterns.

The sites below all offer free and royalty-free images, even for commercial use with the Creative Commons CC0 license.
But some also offer paid or licensed images. If in doubt, check the licenses.

## Free image banks


**[Pixabay](https://pixabay.com/fr/)** - More than 1 million photos, vectorized images and free illustrations for commercial use. Without attribution

![Free and royalty-free images](./img/Images-gratuites-et-libres-de-droits.jpg)

 All content is distributed under Creative Commons CC0, making it safe to use without asking permission or giving credit to the artist - even for commercial purposes.

**[PicJumbo](http://picjumbo.com/)** - High resolution image bank.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-4.jpg)

**[Pexels](https://www.pexels.com/)** - 100 new high resolution images per day.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-14.jpg)

**[Pixabay Editors Choice](http://pixabay.com/en/editors_choice/)** \- Royalty-free photos selected manually

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-10.jpg)

**[MorgueFile](http://www.morguefile.com/)** Free image bank, updated by artists and photographers.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-5.jpg)

**[PhotoPin](http://photopin.com/)** A search engine in the free image banks.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-6.jpg)

**[StockPhotosForFree](http://www.stockphotosforfree.com/)** Free, royalty-free image bank. _Registration required_

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-7.jpg)

**[OpenPhoto](http://openphoto.net/)** Collection of a photographer provided free of charge.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-8.jpg)

**[Free Images](http://www.freeimages.com)** Old-school but still relevant. _Registration required_

![Free and Royalty Free Image Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-1.jpg)

**[500px - Creative Commons](https://500px.com/creativecommons)** The Creative Commons section of the 500px photo sharing site.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-11.jpg)

**[Photober](http://www.photober.com/free-photos/)** Free photos, sorted by category.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-12.jpg)

**[Nasa](https://www.nasa.gov/multimedia/imagegallery/index.html#.Uw56HvRdVDo)** Historical and scientific images.

![Free and Royalty Free Image Banks](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-13.jpg)

## High resolution images

**[Unsplash](http://www.unsplash.com)** High resolution wide images. Perfect for cover images.

![Free unsplash images](./img/Images-gratuites-sur-unplash.jpg)

**[Gratisography](http://www.gratisography.com/)** Private collection of a professional photographer, available free of charge.

![Free images gratisography](./img/Images-gratuites-sur-gratisography.jpg)

**[Little Visuals](http://littlevisuals.co/)** Artistic and beautiful photos. Sort by category and tags.

![Free and Royalty Free Image Bank](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-3.jpg)

**[Wefunction Free Photos](http://wefunction.com/category/free-photos/)** 4 Small collections of images. Free, royalty-free, high resolution.

![Free and Royalty Free Image Bank](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-1.jpg)

**[Getrefe (Tumblr)](http://getrefe.tumblr.com/)** A handful of free pictures

![Images gratuites tumblr](./img/Images-gratuites-sur-tumblr.jpg)

**[Death To Stock](http://deathtothestockphoto.com/)** 10 free images per month in the form of a newsletter.

![Free and Royalty Free Image Bank](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-2.jpg)

## Royalty-free vintage images


**[New Old Stock](http://nos.twnsnd.co/)** - All kinds of vintage photographs.

![Free and Royalty Free Vintage Image Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage.jpg)

**[Flickr - Library of Congress](https://secure.flickr.com/photos/library_of_congress/)** - Historical collection of the Library of Congress (US).

![Free and Royalty Free Vintage Image Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-1.jpg)

**[Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)** - Wikipedia's photo collection.

![Free and Royalty Free Vintage Image Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-5.jpg)

**[US History Images](http://ushistoryimages.com/)** - An orderly collection of images from the history of the United States.

![Free and Royalty Free Vintage Image Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-3.jpg)

**[Secret Museum](http://ian.macky.net/secretmuseum/index.php)** - A collection of images of primitive cultures.

![Free and Royalty Free Vintage Image Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-4.jpg)

## Free image search engines


**[Stock.io](https://www.stockio.com/)** A search engine for free and royalty-free photos, vectors, videos, icons and fonts!

![free image search engine](./img/moteur-de-recherche-dimages-gratuites-1.jpg)

**[Visual Hunt](https://visualhunt.com/)** More than 350 million free images, including 116,000 under CC0 license (Public Domain)

![free image search engine](./img/moteur-de-recherche-dimages-gratuites-2.jpg)

**[Burst](https://burst.shopify.com/)** An initiative of Shopify. Very good quality photos oriented entrepreneurs.

![royalty-free image search engine](./img/moteur-de-recherche-dimages-libres-de-droit.jpg)

**[StickPNG](http://www.stickpng.com/)** A PNG image search engine on a transparent background.

![free image search engine](./img/moteur-de-recherche-dimages-gratuites.jpg)

**[LibreStock](http://librestock.com/)** A search engine of 47 different image banks. Contains more than 60,000 free and royalty-free photos from the public domain.

![free image search engine](./img/moteur-de-recherche-dimages-gratuites-3.jpg)

**[EveryPixel](https://everypixel.com/free)** Free image search engine powered by 50 image banks. Several million photos accessible via this search engine.

![free image search engine](./img/moteur-de-recherche-dimages-gratuites-4.jpg)

## Free Illustrations

**[OpenClipart](https://openclipart.org/)** - Vectors, clip arts and free illustrations.

![Free illustrations](./img/Banques-dImages-Gratuites-et-Libres-de-Droit.svg)

**[Wpclipart](http://www.wpclipart.com/)** More free clip arts. Overall outdated, but contains some pearls.

![Free illustrations](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-1.svg)

**[Public Domain Vectors](http://www.publicdomainvectors.org/)**: Vectors and illustrations of the public domain.

![Free illustrations](./img/strongmanbeach.svg)

**[Public Domain Clip Art](http://www.pdclipart.org/)**: Another public domain site. Free clip arts and illustrations.

![Free illustrations](./img/Banques-dImages-Gratuites-et-Libres-de-Droit.jpg)

## Patterns and Structures
--------------------

**[The Pattern Library.com](http://thepatternlibrary.com/)** - Free patterns for designers.

![Free and Royalty-Free Patterns Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Motifs-1.jpg)

**[Subtle Patterns](http://subtlepatterns.com/)** - More patterns. More subtle.

![Free and Royalty-Free Patterns Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Motifs-2.jpg)

**[Photoshop Patterns – The Ultimate Collection](http://www.smashingmagazine.com/2009/02/12/the-ultimate-collection-of-free-photoshop-patterns/)** A list of quality patterns.

![Free and Royalty-Free Patterns Banks](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Motifs.jpg)

**[HeroPatterns](http://www.heropatterns.com/)** Free patterns in SVG, regularly updated.

![Free patterns and designs](./img/Motifs-et-patterns-gratuits.jpg)

## Turn your Quotes into Images


*   [Notegraphy](https://spark.adobe.com/): Three clicks are enough to create an image with Notegraphy. Write your quote, select a model and publish
*   [quozio.com](http://quozio.com/): A simple picture quote maker, with 28 templates.
*   [curatedquotes.com](http://www.curatedquotes.com): Free tool to create an image from a quote.
*   [ReciteThis](http://www.recitethis.com/): Create images from your quotes using 42 different backgrounds. Possibility to publish them directly on Facebook, Twitter and Tumblr.

Outdated resources

*   [mervikhaums.com](http://www.mervikhaums.com/idea-center/free-cartoon-characters-for-your-blogs-and-social-media-posts): Free collection of Avatars. _Download link not available_



These image banks should be sufficient for the majority of your projects. If you want a high-end offer, you can opt for paid image banks such as [GettyImages](http://www.gettyimages.fr/), [Istockphotos](http://www.istockphoto.com/fr) or [ShutterStock](https://www.shutterstock.com/fr).
