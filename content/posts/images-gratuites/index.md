---
title: 37 Banques d'Images Gratuites et Libres de Droits
author: Micadep
date: 2019-11-19T00:00:00.000Z
excerpt: Vous voulez avoir à un seul endroit les meilleures banques d’images
  pour vos images gratuites, sans risquer de poursuites ? Alors vous allez
  adorer cet article. J’ai listé les 37 meilleures banques d’images gratuites et
  libres de droits
update: 2019-11-19T00:00:00.000Z
slug: images-gratuites
language: fr
hero: images/banques-dimages-gratuites-et-libres-de-droit-motifs.jpg
---
Vous voulez avoir à un seul endroit les meilleures banques d’images pour vos images gratuites, sans risquer de poursuites ? 

Alors vous allez adorer cet article.

J’ai listé les 37 meilleures banques d’images gratuites et libres de droits ainsi que des moteurs de recherche pour trouver vos images vectorielles, Illustrations et motifs.

Les sites ci-dessous offrent tous des images gratuites et libres de droits, même pour usage commercial avec la licence Creative Commons CC0. Mais certains offrent aussi des images payantes ou avec licence. Dans le doute, vérifiez les licences.

1. Banques d’images gratuites
2. Images haute résolution
3. Images vintage libres de droits
4. Moteurs de recherche d’images gratuites
5. Illustrations Gratuites
6. Motifs et Structures
7. Transformez vos Citations en Images

<h2 id="banques-images-gratuites"> Banques d'images gratuites </h2>

- - -

**[Pixabay](https://pixabay.com/fr/)** - Plus de 1 million de photos, images vectorisées et illustrations gratuites pour usage commercial. Sans attribution

![Images gratuites et libres de droits](./img/Images-gratuites-et-libres-de-droits.jpg)

Tous les contenus sont diffusés sous Creative Commons CC0, ce qui les rend sûres à utiliser sans demander l'autorisation ou donner du crédit à l'artiste - même à des fins commerciales.

**[PicJumbo](http://picjumbo.com/)** - Banque d'images de haute résolution.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-4.jpg)

**[Pexels](https://www.pexels.com/)** - 100 nouvelles images haute résolution par jour.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-14.jpg)

**[Pixabay Editors Choice](http://pixabay.com/en/editors_choice/)** - Photos libres de droits sélectionnées manuellement

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-10.jpg)

**[MorgueFile](http://www.morguefile.com/)** Banque d'images gratuite, mise à jour par des artistes et photographes.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-5.jpg)

**[PhotoPin](http://photopin.com/)** Un moteur de recherche dans les banques d'images gratuites.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-6.jpg)

**[StockPhotosForFree](http://www.stockphotosforfree.com/)** Banque d'images gratuites, libres de droits. *Inscription obligatoire*

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-7.jpg)

**[OpenPhoto](http://openphoto.net/)** Collection d'un photographe mis a disposition gratuitement.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-8.jpg)

**[Free Images](http://www.freeimages.com)** Old-school mais toujours d'actualité. *Inscription obligatoire*

![Banques d'Images Gratuites et Libres de Droit](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-1.jpg)

**[500px - Creative Commons](https://500px.com/creativecommons)** La section Creative Commons du site de partage de photos 500px.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-11.jpg)

**[Photober](http://www.photober.com/free-photos/)** Photos gratuites, triées par catégorie

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-12.jpg)

**[Nasa](https://www.nasa.gov/multimedia/imagegallery/index.html#.Uw56HvRdVDo)** Images historiques et scientifiques.

![Banques d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-13.jpg)

## Images haute résolution

- - -

**[Unsplash](http://www.unsplash.com)** Images larges de haute résolution. Parfait pour des images de couverture.

![Images gratuites unsplash](./img/Images-gratuites-sur-unplash.jpg)

**[Gratisography](http://www.gratisography.com/)** Collection privée d'un photographe professionnel, disponible gratuitement.

![Images gratuites gratisography](./img/Images-gratuites-sur-gratisography.jpg)

**[Little Visuals](http://littlevisuals.co/)** Photos artistiques et magnifiques. Tri par catégorie et par tags.

![Banque d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-3.jpg)

**[Wefunction Free Photos](http://wefunction.com/category/free-photos/)** 4 Petites collections d'images. Gratuites, libres de droits, haute résolution.

![Banque d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-1.jpg)

**[Getrefe (Tumblr)](http://getrefe.tumblr.com/)** Une poignée de photos gratuites

![Images gratuites tumblr](./img/Images-gratuites-sur-tumblr.jpg)

**[Death To Stock](http://deathtothestockphoto.com/)** 10 images gratuites par mois sous forme de newsletter.

![Banque d'Images Gratuites et Libres de Droit](./img/Banque-dImages-Gratuites-et-Libres-de-Droit-2.jpg)

## Images vintage libres de droits

- - -

**[New Old Stock](http://nos.twnsnd.co/)** - Toutes sortes de photographies vintage.

![Banques d'Images Gratuites et Libres de Droits Vintage](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage.jpg)

**[Flickr - Library of Congress](https://secure.flickr.com/photos/library_of_congress/)** - Collection historique de la bibliothèque du Congrès (US).

![Banques d'Images Gratuites et Libres de Droits Vintage](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-1.jpg)

**[Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)** - La collection de photos de Wikipédia.

![Banques d'Images Gratuites et Libres de Droit Vintage](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-5.jpg)

**[US History Images](http://ushistoryimages.com/)** - Une collection ordonnée d'images de l'histoire des États-Unis.

![Banques d'Images Gratuites et Libres de Droit Vintage](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-3.jpg)

**[Secret Museum](http://ian.macky.net/secretmuseum/index.php)** - Une collection d'images des cultures primitives.

![Banques d'Images Gratuites et Libres de Droit Vintage](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Vintage-4.jpg)

## Moteurs de recherche d'images gratuites

- - -

**[Stock.io](https://www.stockio.com/)** Un moteur de recherche de photos, vecteurs, vidéos, icônes et police gratuites et libres de droits !

![moteur de recherche d'images gratuites](./img/moteur-de-recherche-dimages-gratuites-1.jpg)

**[Visual Hunt](https://visualhunt.com/)** Plus de 350 millions d'images gratuites, dont 116 000 sous licence CC0 (Domaine Public)

![moteur de recherche d'images gratuites](./img/moteur-de-recherche-dimages-gratuites-2.jpg)

**[Burst](https://burst.shopify.com/)** Une initiative de Shopify. Photos de très bonne qualité orientées entrepreneurs.

![moteur de recherche d'images libres de droit](./img/moteur-de-recherche-dimages-libres-de-droit.jpg)

**[StickPNG](http://www.stickpng.com/)** Un moteur de recherche d'images PNG sur fond transparent.

![moteur de recherche d'images gratuites](./img/moteur-de-recherche-dimages-gratuites.jpg)

**[LibreStock](http://librestock.com/)** Un moteur de recherche de 47 banques d'images différentes. Contiens plus de 60 000 photos gratuites et libres de droits du domaine public.

![moteur de recherche d'images gratuites](./img/moteur-de-recherche-dimages-gratuites-3.jpg)

**[EveryPixel](https://everypixel.com/free)** Moteur de recherche d'images gratuites propulsé par 50 banques d'images. Plusieurs millions de photos accessibles via ce moteur de recherche.

![moteur de recherche d'images gratuites](./img/moteur-de-recherche-dimages-gratuites-4.jpg)

## Illustrations Gratuites

- - -

**[OpenClipart](https://openclipart.org/)** - Vecteurs, clip arts et illustrations gratuites.

![Illustrations gratuites](./img/Banques-dImages-Gratuites-et-Libres-de-Droit.svg)

**[Wpclipart](http://www.wpclipart.com/)** Encore des clip arts gratuits. Globalement démodé, mais contient quelques perles.

![Illustrations gratuites](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-1.svg)

**[Public Domain Vectors](http://www.publicdomainvectors.org/)**: Vecteurs et illustrations du domaine public.

![Illustrations gratuites](./img/strongmanbeach.svg)

**[Public Domain Clip Art](http://www.pdclipart.org/)**: Encore un site du domaine public. clip arts et illustrations gratuites.

![Illustrations gratuites](./img/Banques-dImages-Gratuites-et-Libres-de-Droit.jpg)

## Motifs et Structures

- - -

**[The Pattern Library.com](http://thepatternlibrary.com/)** - Motifs gratuits pour designers.

![Banques d'Images Gratuites et Libres de Droit Motifs](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Motifs-1.jpg)

**[Subtle Patterns](http://subtlepatterns.com/)** - Plus de motifs. Plus subtile.

![Banques d'Images Gratuites et Libres de Droit Motifs](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Motifs-2.jpg)

**[Photoshop Patterns – The Ultimate Collection](http://www.smashingmagazine.com/2009/02/12/the-ultimate-collection-of-free-photoshop-patterns/)** Une liste de motifs de qualité.

![Banques d'Images Gratuites et Libres de Droit Motifs](./img/Banques-dImages-Gratuites-et-Libres-de-Droit-Motifs.jpg)

**[HeroPatterns](http://www.heropatterns.com/)** Motifs gratuits en SVG, régulièrement mis à jour.

![Motifs et patterns gratuits](./img/Motifs-et-patterns-gratuits.jpg)

## Transformez vos Citations en Images

- - -

* [Notegraphy](https://spark.adobe.com/): Trois cliques suffisent pour créer une image avec Notegraphy. On Écris sa citation, on sélectionne un modèle et on publie
* [quozio.com](http://quozio.com/): A simple picture quote maker, with 28 templates.
* [curatedquotes.com](http://www.curatedquotes.com): Outil gratuit pour créer une image à partir d'une citation.
* [ReciteThis](http://www.recitethis.com/): Créez des images a partir de vos citations a l'aide de 42 differents fonds. Possibilité de les publier directement Facebook, Twitter et Tumblr.

Ressources Obsolètes

* [mervikhaums.com](http://www.mervikhaums.com/idea-center/free-cartoon-characters-for-your-blogs-and-social-media-posts): Collection gratuite d'Avatars. *Lien de téléchargement indisponible*

Ces banques d’images devraient suffire pour la majorité de vos projets. Si vous souhaitez une offre haut de gamme, vous pouvez opter pour des banques d'images payantes tel que [GettyImages](http://www.gettyimages.fr/), [Istockphotos](http://www.istockphoto.com/fr) ou [ShutterStock](https://www.shutterstock.com/fr).